bandas = ["Aviões do forró", "Solange Almeida", "Márcia Fellipe"]

# puts bandas [0]
# puts bandas [1]
# puts bandas [2]


bandas.push ("Garota Safada") #método que adiciona mais uma banda no array

# puts bandas [3]

bandas.delete ("Aviões do forró") #método que exclui banda no array

# puts bandas

# solotop = bandas.find { |item| item == 'Solange Almeida' } #método de busca

solotop = bandas.find { |item| item.include?('Almeida') } #método que retorna V ou F

puts solotop