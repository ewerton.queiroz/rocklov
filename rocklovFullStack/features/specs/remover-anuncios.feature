#language: pt

#é a definição do cucumber para trabalhar com gherkin em portugues

Funcionalidade: Remover Anuncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anuncio
    Para que possa manter dashboard atualizado

    Contexto: Login
        * Login com "spider@hotmail.com" e "pwd123"

    #uso de APIs servem para evitar utilizar o cadastro por exemplo.

    @temp
    Cenario: Remover um anuncio

        Dado que eu tenha um anuncio indesejado:
            |   thumb       | telecaster.jpg |
            |   nome        | Telecaster     |
            |categoria      | Cordas         |
            |preco          | 50             |
        Quando eu solicito a exclusão desse item
            E confirmo a exclusão
        Então não devo ver esse item no Dashboard 

     @temp2
    Cenario: Desistir da exclusao
        Dado que eu tenha um anuncio indesejado:
            |   thumb       | conga.jpg      |
            |   nome        | conga          |
            |categoria      | Outros         |
            |preco          | 100            |
        Quando eu solicito a exclusão desse item
            Mas não confirmo a solicitação
        Então esse item deve permanecer no meu Dashboard 