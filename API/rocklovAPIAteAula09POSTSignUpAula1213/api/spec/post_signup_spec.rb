require_relative "routes/signup"
require_relative "libs/mongo"

describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Ze", email: "ze@gmail.com", password: "ufcg1234" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      #dado que eu tenho um novo usuario
      payload = { name: "Joao da Silva", email: "joao@gmail.com", password: "ufcg1234" }
      MongoDB.new.remove_user(payload[:email])

      #e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisicao para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      #entao deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end
end
