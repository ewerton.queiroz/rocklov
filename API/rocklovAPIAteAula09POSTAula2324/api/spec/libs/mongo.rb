require "mongo"

Mongo::Logger.logger = Logger.new("./logs/mongo.log") #evita criar logs desnecessário na saída do terminal

class MongoDB #serve para fazer a exclusão dos emails gerados no banco Robo 3T
  attr_accessor :users, :equipos

  def initialize
    client = Mongo::Client.new("mongodb://rocklov-db:27017/rocklov")
    @users = client[:users]
    @equipos = client[:equipos]
  end

  def remove_user(email)
    @users.delete_many({ email: email })
  end

  def get_user(email)
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, user_id)
    obj_id = BSON::ObjectId.from_string(user_id)

    @equipos.delete_many({ name: name, user: obj_id })
  end

  def get_mongo_id #retorna id que nao é valido, nao existe no banco equipo c esse id
    return BSON::ObjectId.new
  end
end
