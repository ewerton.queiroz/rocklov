describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "pb@gmail.com", password: "ufcg1234" }
    result = Sessions.new.login(payload)
    @eb_id = result.parsed_response["_id"]
  end

  context "solicitar locacao" do
    before(:all) do

      #dado que wc tem uma "sanfona" para locação

      #login
      result = Sessions.new.login({ email: "bt@gmail.com", password: "ufcg1234" })
      wc_id = result.parsed_response["_id"]

      sanfona = {
        thumbnail: Helpers::get_thumb("sanfona.jpg"),
        name: "sanfona",
        category: "Outros",
        price: 499,
      }

      MongoDB.new.remove_equipo(sanfona[:name], wc_id)

      #cadastrar sanfona para wc
      result = Equipos.new.create(sanfona, wc_id)
      sanfona_id = result.parsed_response["_id"]

      #quando solicito a locação da fender do Joe Perry
      @result = Equipos.new.booking(sanfona_id, @eb_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
