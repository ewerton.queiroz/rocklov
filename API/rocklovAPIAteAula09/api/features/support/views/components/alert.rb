


class Alert
    include Capybara::DSL

    #componente presente tanto em login como cadastro
    def dark
        return find(".alert-dark").text
    end
end