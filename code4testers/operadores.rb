# Operadores Matemáticos

def soma(n1, n2)
    puts n1 + n2       #operador de soma
end

def subtrai(n1, n2)
    puts n1 - n2       #operador de subtração
end

def multiplica(n1, n2)
    puts n1 * n2       #operador de multiplicação
end

def divide(n1, n2)
    resultado = (n1.to_f / n2.to_f).round(3)       #operador de divisão para utilizar números com virgula deve-se usar float
    puts resultado
    puts resultado.class
end

soma(2, 3)
subtrai(2, 3)
multiplica(2, 5)
divide(11, 3)