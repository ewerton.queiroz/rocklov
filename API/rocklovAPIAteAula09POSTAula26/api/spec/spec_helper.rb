require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"

require_relative "libs/mongo"
require_relative "helpers"

require "digest/md5" #converter password p criptografia md5

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.before(:suite) do #semente de cadastro inicial - pré-condição
    users = [
      { name: "Ewerton Brasil", email: "eb@hotmail.com", password: to_md5("ufcg1234") },
      { name: "Emerson Silva", email: "eb2@gmail.com", password: to_md5("ufcg1234") },
      { name: "Wallysson Costa", email: "wc@gmail.com", password: to_md5("ufcg1234") },
      { name: "Pedro Brasil", email: "pb@gmail.com", password: to_md5("ufcg1234") },
      { name: "Bernardo Tabosa", email: "bt@gmail.com", password: to_md5("ufcg1234") },
    ]

    MongoDB.new.drop_danger #apaga todos usuarios do banco
    MongoDB.new.insert_users(users) #inseri todos da semente
  end
end
