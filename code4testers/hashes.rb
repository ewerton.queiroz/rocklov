#estruturas de dados parecidas com array, guardam conjunto de dados; diferença: 
# posições endereçadas através de chave;

Ewerton = {nome: "Ewerton", email: "ewerton.queiroz@ee.ufcg.edu.br", legal: true}
# puts Ewerton[:nome]
# puts Ewerton[:email]
# puts Ewerton[:legal]

Joao = {nome: "Joao", email: "joao@yahoo.com.br", legal: true}
# puts Joao[:nome]
# puts Joao[:email]
# puts Joao[:legal]

# hashes usados para manipulação de massa de testes, definição de massa de testes;

pessoas = [Ewerton, Joao]

puts pessoas[0][:nome]
puts pessoas[1][:email]

#adicionando hashes no array, variaveis pessoas é um array de hashes;