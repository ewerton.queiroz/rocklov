#op1

# 10.times do              # Executando laços de repetição
#     puts "Executando o times" 
# end

#op2
# i = 1
# 10.times do
#     puts "Execução numero #{i}"
#     i=i+1
# end

bandas = ["Solange Almeida", "Márcia Fellipe", "Wesley Safadão", "Maria Bethânia"]

i = 0
bandas.size.times do
    puts bandas[i]
    i = i + 1
end
