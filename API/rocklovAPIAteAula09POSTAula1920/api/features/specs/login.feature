#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "eb@gmail.com" e "ufcg1234"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input  | senha_input | mensagem_output                  |
            | eb@gmail.com | abcde       | Usuário e/ou senha inválidos.    |
            | ea@gmail.com | ufcg1234    | Usuário e/ou senha inválidos.    |
            | eb&gmail.com | ufcg1234    | Oops. Informe um email válido!   |
            |              | ufcg1234    | Oops. Informe um email válido!   |
            | eb@gmail.com |             | Oops. Informe sua senha secreta! |