require_relative "base_api"

class Equipos < BaseApi # <BaseApi representa a herança
  def create(payload, user_id)

    #self.class serve p ter acesso aos objetos da própria classe
    return self.class.post(
             "/equipos",
             body: payload,
             headers: {
               "user_id": user_id,
             },
           )
  end
end
