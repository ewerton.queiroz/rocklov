# #Exemplo da carteira de motorista - CNH

# puts "Qual o seu nome?"
# nome = gets.chomp   #gets quebra a linha automaticamente, chomp que concatena as strings
# puts "Informe sua idade"
# idade = gets.chomp.to_i


# if (idade >= 18)
#     puts nome + ", você tem " + idade.to_s + " anos e portanto pode tirar sua carteira de motorista."
# elsif (idade >= 7)
#     puts nome + ", você tem " + idade.to_s + " anos e não pode tirar sua carteira de motorista."
# else 
#     puts nome + ", você tem " + idade.to_s + " anos e só pode andar de velocípe."
# end

#Exemplo da carteira de motorista - CNH

# encoding UTF-8

puts "Qual o seu nome?"
nome = gets.chomp   #gets quebra a linha automaticamente, chomp que concatena as strings
puts "Informe sua idade"
idade = gets.chomp.to_i


if (idade >= 18)
    puts "#{nome}, você tem #{idade} anos e portanto pode tirar sua carteira de motorista."
elsif (idade >= 7)
    puts "#{nome}, você tem #{idade} anos e não pode tirar sua carteira de motorista."
elsif (idade >=1)
    target = "anos"
    target = "ano" if (idade ==1)
    puts "#{nome}, você tem #{idade} #{target} e só pode andar de velocípe."
    puts "Opps! Idade Incorreta, tente novamente."
end