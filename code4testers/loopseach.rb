#mesma coisa do looptimes, sendo que otimizado com foreach; código enxuto e clean;
#sempre que usar array e loops, aconselhável utilizar foreach;

bandas = ["Solange Almeida", "Márcia Fellipe", "Wesley Safadão", "Maria Bethânia", "Zé Vaqueiro"]

bandas.each do |banda|
    puts banda
end