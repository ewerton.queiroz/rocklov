

#nome, marca, modelo, cor, quatdade de portas, etc..
#Ligar, businar, parar, etc..

#Uma classe é um objeto que tem características e funções

class Carro
    attr_accessor :nome, :cor, :marca

    def ligar
        puts "O #{@nome} está pronto para iniciar o trajeto."
    end
end

#para usar a classe é preciso instanciar

chevette = Carro.new

chevette.nome = "Chevette"
chevette.cor  = "Bege"
chevette.marca = "GM"

chevette.ligar
puts chevette.class

fusca = Carro.new

fusca.nome = "Fusca"
fusca.cor  = "Azul"
fusca.marca = "VW"

fusca.ligar
