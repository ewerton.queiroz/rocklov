def maior_que(v1, v2)
    puts v1 > v2           #sempre retorna true ou false
end

#retorna true, porque 12 é maior que 2
maior_que(12, 2)

#retorna false, porque 2 não é maior que 12
maior_que(2,12)

#retorna false, porque 10 não é maior que 10, é igual
maior_que(10,10)


def menor_que(v1, v2)
    puts v1 < v2          
end

#retorna true, porque 2 é menor que 10
menor_que(2,10)


def maior_ou_igual_que(v1, v2)
    puts v1 >= v2
end

#retorna true, porque 10 é igual a 10
maior_ou_igual_que(10, 10)

#retorna true, porque 15 é maior que 10
maior_ou_igual_que(15,10)


def menor_ou_igual_que(v1, v2)
    puts v1 <= v2
end

#retorna true porque 5 é igual a 5
menor_ou_igual_que(5,5)

#retorna true porque 5 é menor que 10
menor_ou_igual_que(5,10)

#retorna false porque 10 não é menor que 5
menor_ou_igual_que(10,5)