

#nome, marca, modelo, cor, quatdade de portas, etc..
#Ligar, businar, parar, etc..

#Uma classe é um objeto que tem características e funções

class Carro
    attr_accessor :nome, :cor, :marca

    def initialize(nome, marca) #construtor dentro da classe ruby, invocado e executado automat. sempre que a classe é inicializada
        @nome = nome 
        @marca = marca
    end

    def ligar
        puts "O #{@nome} está pronto para iniciar o trajeto."
    end
end

#para usar a classe é preciso instanciar
chevette = Carro.new("Chevette", "GM") #("Fusca") é definido de acordo com o metodo construtor
chevette.ligar

fusca = Carro.new("Fusca", "VW") #("Fusca") é definido de acordo com o metodo construtor
fusca.ligar
