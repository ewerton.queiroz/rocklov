class LoginPage
  include Capybara::DSL #classe precisa conhecer todos recursos do capybara

  def open
    visit "/"
  end

  def with(email, password)
    find("input[placeholder='Seu email']").set email
    find("input[type=password]").set password
    click_button "Entrar"
  end

  # def campo_email
  #     return find ("input[placeholder='Seu e-email']")
  # end

  # def campo_senha
  #     return find ("input[type=password]")
  # end

  # def botao_entrar
  #     click_button "Entrar"
  # end

end
