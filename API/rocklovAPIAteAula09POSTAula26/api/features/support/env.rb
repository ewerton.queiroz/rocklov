require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"

CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))
# BROWSER = ENV["BROWSER"]

# if BROWSER == "firefox"
#   @driver = :selenium
# elsif BROWSER == "fire_headless"
#   @driver = :selenium_headless
# elsif BROWSER == "chrome"
#   @driver = :selenium_chrome
# else
#   @driver = :selenium_chrome_headless
# end

case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "chrome"
  @driver = :selenium_chrome
when "fire_headless"
  @driver = :selenium_headless
when "chrome_headless"
  @driver = :selenium_chrome_headless
else
  raise "Navegador incorreto, variavel @driver está vazia :("
end

Capybara.configure do |config|
  config.default_driver = @driver
  #config.default_driver = :selenium_chrome_headless #apenas no mozila para chroma add _chrome
  # config.app_host = "http://rocklov-web:3000" #URL Padrão
  config.app_host = CONFIG["url"]

  config.default_max_wait_time = 10 #timeout implicito do capybara
end

AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true
end
