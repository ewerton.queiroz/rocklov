Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable

  # log table.hashes #construido p entender como cucumber converte tabela table pipe para objeto ruby
  user = table.hashes.first #obtem primeiro item da tabela, convertendo uma tabela p array do ruby
  # log user

  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)
end
