require_relative "base_api"

class Signup < BaseApi # <BaseApi representa a herança
  def create(payload)

    #self.class serve p ter acesso aos objetos da própria classe
    return self.class.post(
             "/signup",
             body: payload.to_json,
             headers: {
               "Content-Type": "application/json",
             },
           )
  end
end
