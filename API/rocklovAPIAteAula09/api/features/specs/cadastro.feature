#language: pt

#da aula 16

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome           | email        | senha    |
            | Ewerton Brasil | eb@gmail.com | ufcg1234 |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input     | email_input  | senha_input | mensagem_output                  |
            |                | ed@gmail.com | ufcg1234    | Oops. Informe seu nome completo! |
            | Ewerton Brasil |              | ufcg1234    | Oops. Informe um email válido!   |
            | Ewerton Brasil | ed*gmail.com | ufcg1234    | Oops. Informe um email válido!   |
            | Ewerton Brasil | ed&gmail.com | ufcg1234    | Oops. Informe um email válido!   |
            | Ewerton Brasil | ed@gmail.com |             | Oops. Informe sua senha secreta! |
