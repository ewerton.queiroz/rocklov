

#nome, marca, modelo, cor, quatdade de portas, etc..
#Ligar, businar, parar, etc..

#Uma classe é um objeto que tem características e funções

class Veiculo
    attr_accessor :nome, :cor, :marca

    def initialize(nome, marca) #construtor dentro da classe ruby, invocado e executado automat. sempre que a classe é inicializada
        @nome = nome 
        @marca = marca
    end

    def define_cor(cor)
        @cor = cor
    end
end

class Carro < Veiculo
    def ligar
        puts "O #{@nome} está pronto para iniciar o trajeto."
    end

    def dirigir
        puts "O #{@nome} está iniciando o trajeto."
    end

end

class Moto < Veiculo
    def ligar
        puts "A #{@nome} está pronta para iniciar o trajeto."
    end

    def pilotar
        puts "A #{@nome} está iniciando o trajeto."
    end
end

#para usar a classe é preciso instanciar
chevette = Carro.new("Chevette", "GM") #("Fusca") é definido de acordo com o metodo construtor
chevette.ligar
chevette.dirigir
chevette.define_cor("Bege")


fusca = Carro.new("Fusca", "VW") #("Fusca") é definido de acordo com o metodo construtor
fusca.ligar
fusca.dirigir
fusca.define_cor("Azul")

cg = Moto.new("CG", "Honda")
cg.ligar
cg.pilotar
cg.define_cor("Rosa")