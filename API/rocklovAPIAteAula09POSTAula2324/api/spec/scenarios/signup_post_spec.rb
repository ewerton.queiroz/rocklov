describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Ze", email: "ze@gmail.com", password: "ufcg1234" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      #dado que eu tenho um novo usuario
      payload = { name: "Joao da Silva", email: "joao@gmail.com", password: "ufcg1234" }
      MongoDB.new.remove_user(payload[:email])

      #e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisicao para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      #entao deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  examples = [
    {
      title: "Campo nome em branco",
      payload: { name: "", email: "joao@gmail.com", password: "ufcg1234" },
      code: 412,
      error: "required name",
    },
    {
      title: "Sem campo nome",
      payload: { email: "joao@gmail.com", password: "ufcg1234" },
      code: 412,
      error: "required name",
    },
    {
      title: "Campo email invalido",
      payload: { name: "Joao da Silva", email: "joao#gmail.com", password: "ufcg1234" },
      code: 412,
      error: "wrong email",
    },
    {
      title: "Campo email em branco",
      payload: { name: "Joao da Silva", email: "", password: "ufcg1234" },
      code: 412,
      error: "required email",
    },
    {
      title: "Sem Campo email",
      payload: { name: "Joao da Silva", password: "ufcg1234" },
      code: 412,
      error: "required email",
    },
    {
      title: "Campo senha em branco",
      payload: { name: "Joao da Silva", email: "joao@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "Sem Campo senha",
      payload: { name: "Joao da Silva", email: "joao@gmail.com" },
      code: 412,
      error: "required password",
    },
  ]
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
