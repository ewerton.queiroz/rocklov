def igual (v1, v2)
    puts v1 == v2
end


#retorna true, porque é igual
igual(10, 10)

#retorna false, porque é diferente
igual(12, 11)

#retorna false, porque os valores iguais mas tipos diferentes
igual(10, "10")


def diferente(v1, v2)
    puts v1 != v2
end


#retorna false, porque 10(inteiro) é diferente de 10(string)
diferente(10, "10")


#retorna false, porque ambos são iguais
diferente(10, 10)

#retorna true, porque são diferentes
diferente(5, 10)