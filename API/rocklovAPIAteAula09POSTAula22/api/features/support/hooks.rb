Before do
  @login_page = LoginPage.new
  @alert = Alert.new
  @signup_page = SignupPage.new
  @dash_page = DashPage.new
  @equipos_page = EquiposPage.new

  # page.driver.browser.manage.window.maximize
  page.current_window.resize_to(1440, 900)
end

After do
  # page.save_screenshot("debug/ultimo-teste.png")
  #é um gancho para depois do desfecho, anexando imagens no relatório allure
  temp_shot = page.save_screenshot("logs/temp_screenshot.png")

  Allure.add_attachment(
    name: "Screenshot",
    type: Allure::ContentType::PNG,
    source: File.open(temp_shot),

  )
end

# def teste(valor)
#   puts valor
# end
