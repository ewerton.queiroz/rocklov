require "routes/sessions"
require_relative "helpers"
# DRY Don't Repeat Yourself => Não se Repita
# massa de teste explícita na camada de script - boa prática

describe "POST /sessions" do #com describe crio uma suite
  context "login com sucesso" do
    before(:all) do #adicionar o :all serve para o before rodar apenas 1 vez no contesto
      payload = { email: "eb@gmail.com", password: "ufcg1234" }

      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24 #length serve para contar qtde de caracteres
      #mongodb sempre vai ter 24 caracteres o id, é padrão.
    end
  end

  # examples = [
  #   {
  #     title: "senha incorreta",
  #     payload: { email: "eb@gmail.com", password: "ufcg1235" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "usuario nao existe",
  #     payload: { email: "ebc@gmail.com", password: "ufcg1235" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "email em branco",
  #     payload: { email: "", password: "ufcg1235" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "sem o campo email",
  #     payload: { password: "ufcg1235" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "senha em branco",
  #     payload: { email: "eb@gmail.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "sem o campo senha",
  #     payload: { email: "eb@gmail.com" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  #puts examples.to_json

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end

  #   puts result.parsed_response["_id"] #dados da requisição sendo mostrados em tela - resultado
  #   puts result.parsed_response.class #dados da requisição sendo mostrados em tela - tipo
end
